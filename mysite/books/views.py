from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import BankdetailsForm

from .models import Bankdetails
from django.shortcuts import render, get_object_or_404


def bank_list(request):
    banks = Bankdetails.objects.all()
    return render(request, 'books/bank_list.html', {'banks': banks})


def save_bank_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            banks = Bankdetails.objects.all()
            data['html_bank_list'] = render_to_string('includes/partial_bank_list.html', {
                'banks': banks
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def bank_create(request):
    if request.method == 'POST':
        form = BankdetailsForm(request.POST)
    else:
        form = BankdetailsForm()
    return save_bank_form(request, form, 'includes/bank_create.html')


def bank_update(request, pk):
    bank = get_object_or_404(Bankdetails, pk=pk)
    if request.method == 'POST':
        form = BankdetailsForm(request.POST, instance=bank)
    else:
        form = BankdetailsForm(instance=bank)
    return save_bank_form(request, form, 'includes/bank_update.html')


def bank_delete(request, pk):
    print(pk)
    bank = get_object_or_404(Bankdetails, pk=pk)
    data = dict()
    if request.method == 'POST':
        bank.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        banks = Bankdetails.objects.all()
        data['html_bank_list'] = render_to_string('includes/partial_bank_list.html', {
            'banks': banks
        })
    else:
        context = {'bank': bank}
        data['html_form'] = render_to_string('includes/bank_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)
