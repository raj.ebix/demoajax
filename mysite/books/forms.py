from django import forms
from .models import Bankdetails

class BankdetailsForm(forms.ModelForm):
    class Meta:
        model = Bankdetails
        fields = ('beneficiaryName','accountNo','ifscCode','monthlyAmount','maxAmountCount' )

