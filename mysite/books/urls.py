
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('banks/',  views.bank_list, name='bank_list'),
    path('bank/create/', views.bank_create, name='bank_create'),
    path('bank/<int:pk>/update/', views.bank_update, name='bank_update'),
    path('bank/<int:pk>/delete/', views.bank_delete, name='bank_delete'),
]


