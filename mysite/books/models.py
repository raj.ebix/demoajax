from django.db import models


# Create your models here.
class Bankdetails(models.Model):
    beneficiaryId = models.IntegerField(default=0)
    beneficiaryName = models.CharField(max_length=100)
    accountNo = models.CharField(max_length=20, unique=True)
    ifscCode = models.CharField(max_length=12)
    monthlyAmount = models.DecimalField(max_digits=8, decimal_places=2)
    maxAmountCount = models.DecimalField(max_digits=8, decimal_places=2)
    isActive = models.BooleanField(default=True)
    deactivateDatetime = models.DateField(auto_now_add=True)
    addedDatetime = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.beneficiaryName
